<?php

use yii\db\Migration;

class m160527_084644_init extends Migration
{
    public function up()
    {
        $this->createTable('user',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'balance' => $this->decimal(10,2)
        ]);

        $faker = \Faker\Factory::create();
        for($i=0;$i<10;$i++) {
            $this->execute("INSERT INTO \"user\" (\"name\",balance) VALUES('".pg_escape_string($faker->name)."', ".(rand(50000,10000000)/100).")");
        }

    }

    public function down()
    {
        $this->dropTable('user');
    }

}
