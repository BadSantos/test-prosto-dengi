##### Task 1
``` sql 
SELECT  u.name,
    	(SELECT text
	       FROM "comments" c
	      WHERE c.user_id = u.id
	      ORDER by id DESC
	      LIMIT 1
	    ) as last_comment
  FROM "user" u
```

##### Task 2
##### Requirements
* PostgreSQL
* PHP 5.4+

##### Install:
1. `composer global require "fxp/composer-asset-plugin:~1.1.1"`
2. `composer create-project --prefer-dist badsantos/test-prosto-dengi --repository="{\"type\": \"vcs\",\"url\": \"https://bitbucket.org/BadSantos/test-prosto-dengi.git\"}"`
3. Configure db connection:
``` php
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=127.0.0.1 port=5434 dbname=test',//change connection string
            'username' => 'root',//set username
            'password' => '',//set password
            'charset' => 'utf8',
        ],
```
4. `yiic migrate`
##### Move money from user to user. Usage:
1. `yiic pay --userFrom=*** --userTo=*** --amount=****`