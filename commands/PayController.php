<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * Manage with pays
 * @author BadSantos <peregud_roma@mail.ru>
 */
class PayController extends Controller
{
    /**
     * @var int id-user from
     */
    public $userFrom;

    /**
     * @var int id-user to
     */
    public $userTo;

    /**
     * @var int amount pay
     */
    public $amount;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return ['userFrom', 'userTo', 'amount'];
    }

    /**
     * Pays amount from userFrom to userTo
     */
    public function actionIndex()
    {
        Yii::info('Start pay');
        $this->userFrom = (int)$this->userFrom;
        $this->userTo = (int)$this->userTo;
        try {
            if (empty($this->userFrom) || !$this->isUserExist($this->userFrom)) {
                throw new \InvalidArgumentException('UserFrom is not set or not exist');
            } else {
            }
            if (empty($this->userTo) || !$this->isUserExist($this->userTo)) {
                throw new \InvalidArgumentException('UserTo is not set or not exist');
            }
            if ($this->userFrom == $this->userTo) {
                throw new \InvalidArgumentException('UserFrom and UserTo is equal');
            }
            if (empty($this->amount)) {
                throw new \InvalidArgumentException('Amount is not set');
            }
        } catch (\InvalidArgumentException $ex) {
            $message = 'Invalid arguments in pay/index: ' . $ex->getMessage();
            $this->stderr($message);
            Yii::error($message);
            return self::EXIT_CODE_ERROR;
        }

        try {
            Yii::trace('Start transaction');
            Yii::$app->db->transaction(function ($db) {
                /** @var $db \yii\db\Connection */
                $fromUserRow = $db->createCommand('SELECT * FROM "user" WHERE id = :id', [':id' => $this->userFrom])->queryOne();
                if ($fromUserRow['balance'] < $this->amount) {
                    throw new \Exception('User from has no enought money');
                }
                $db->createCommand('UPDATE "user" SET balance = (balance - ' . $this->amount . ') WHERE id = :id', [':id' => $this->userFrom])->execute();
                $db->createCommand('UPDATE "user" SET balance = (balance + ' . $this->amount . ') WHERE id = :id', [':id' => $this->userTo])->execute();
            });
            Yii::trace('End transaction');
        } catch (\Exception $ex) {
            $message = 'Transaction failed: ' . $ex->getMessage();
            $this->stderr($message);
            Yii::error($message);
            return self::EXIT_CODE_ERROR;
        }

        $this->stdout('Successfull pay from user #' . $this->userFrom . ' to user #' . $this->userTo . '. Amount: ' . $this->amount);
        Yii::info('Successfull pay');
    }

    /**
     * Method check if user with $userId is exist
     * @param int $userId user id
     * @return bool
     */
    protected function isUserExist($userId)
    {
        return Yii::$app->db->createCommand('SELECT id FROM "user" WHERE id = :id', [':id' => $userId])->queryScalar();
    }
}
